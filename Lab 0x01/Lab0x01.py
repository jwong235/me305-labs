""" 
@file main.py
@brief Changes between LED light patterns when blue button is pressed   
@details The script changes the brightness of LED LD2 on the nucleo board 
between 3 patterns: Square Wave, Sine Wave, and Sawtooth Wave. 
We used the utime, pyb and math modules to achieve this. 
We have included a state transition diagram in our code to reference the logic
 and sequence of how our while loop works.
LINK TO FSM:https://imgur.com/a/WiLL1Vl
LINK TO VIDEO OF LED: https://youtu.be/_7bUtXBwHOQ
@author: Kyle Hammer and Jacob Wong
"""
import time
import math
import utime
import pyb

def reset_timer():
    '''
    @brief      Returns a new start time for the timer
    @details    using the utime block, the function returns the current tick
                count, which we will use as a new reference point to compare
                future tick counts to.
    '''
    startTime = utime.ticks_ms()
    return startTime
    
def update_timer(startTime):
    '''
    @brief      Returns the current time for the timer
    @details    this function finds the difference between the current tick 
                count and the reference point defined by reset_timer() 
                function to find how much time elapsed.
    '''
    current_time = (utime.ticks_diff(utime.ticks_ms(), startTime))/1000
    return current_time
    
def update_stw(current_time):
    '''
    @brief      Runs the LED in the Sawtooth Wave pattern
    @details    Function provides values that will later be converter to the
                brightness of the LED, creating a sawtooth waveform
    @param      current_time, value returned by the update_timer function
    '''
    return (100*(current_time % 1.0))
    
def update_sqw(current_time):
    '''
    @brief      Runs the LED in the Square Wave pattern
    @details    Function provides values that will later be converter to the
                brightness of the LED, creating a square waveform
    @param      current_time, value returned by the update_timer function
    '''
    return 100*(current_time % 1.0 < 0.5)

def update_sw(current_time):
    '''
    @brief      Runs the LED in the Sine Wave pattern
    @details    Function provides values that will later be converter to the
                brightness of the LED, creating a sine waveform
    @param      current_time, value returned by the update_timer function
    '''
   return 100*(0.5*math.sin(current_time*math.pi/5)+0.5)

def onButtonPressFCN(IRQ_src):
    '''
    @brief      Sets button_pressed variable to true when user presses button
    '''
    global button_pressed
    button_pressed = True
    

if __name__== '__main__':
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    #Links LED to pin A5
    pinC13=pyb.Pin (pyb.Pin.cpu.C13)
    #Links Button to pin C13
    tim2 = pyb.Timer(2, freq = 20000)
    #Establishes timer
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    #Links LED to timer for PWM
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    #creates external interupt calling onButtonPressFCN function
    
    state=0
    runs=0
    button_pressed = False
    #sets initial conditions
    
    while(True):
        try:
            if state == 0:
                print ('Welcome, please press button B1 to cycle through LED patterns!')
                state=1
                #immediately switches from state 1 to state 2 after printing
                #the welcome statement
                
            elif state == 1:
                #state 1 is an idle state waiting for button press
                if button_pressed:
                    state=2
                    button_pressed = False
                    start = reset_timer()
                    print ('Square Wave pattern selected')
                    #upon button press, switch to state 2
            elif state == 2:
                if button_pressed == False:
                    t2ch1.pulse_width_percent(update_sqw(update_timer(start)))
                    #constantly updates brightness of LED in a square wave
                    
                if button_pressed == True:
                    state = 3
                    button_pressed = False
                    start = reset_timer()
                    print ('Sin Wave pattern selected')
                    #upon button press, switch to state 3
                    
            elif state == 3:
                if button_pressed == False:
                    t2ch1.pulse_width_percent(update_sw(update_timer(start)))
                    #constantly updates brightness of LED in a sine wave
                    
                if button_pressed == True:
                    state = 4
                    button_pressed = False
                    start = reset_timer()
                    print ('Sawtooth Wave pattern selected')
                    #upon button press, switch to state 4
                    
            elif state == 4: 
                if button_pressed == False:
                    t2ch1.pulse_width_percent(update_stw(update_timer(start)))
                    #constantly updates brightness of LED in a sawtooth wave
                    
                if button_pressed == True:
                    state = 2
                    button_pressed = False
                    start = reset_timer()
                    print ('Square Wave pattern selected')
                    #upon button press, switch to state 2
                    
            runs += 1
            #increases run count by 1
            time.sleep(0.1)
            #delays each while loop cycle by 0.1 seconds
        except KeyboardInterrupt:
                break
            
print ('Program terminating')
#upon KeyboardInterrupt, program prints terminating message and closes program         
        