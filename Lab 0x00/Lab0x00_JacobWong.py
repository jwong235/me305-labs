# -*- coding: utf-8 -*-
"""@filename Lab0x00JacobWong.py
Created on Thu Sep 23 15:59:16 2021

@author: Jacob Wong
"""

def fib (idx):
    #This function calculates a Fibonacci number for a given index.
    #The imput parameter, or the index, must be a nonnegative integer
    if idx == 0:
        return 0
    #Lines 11 and 12 checks if the index is 0 and defines the Fibonacci
    #number of index 0 to be 0.
    elif idx == 1:
        return 1
    #Lines 15 and 16 checks if the index is 1 and defines the Fibonacci
    #number of index 1 to be 1.
    else:
    #If the index is more than 1, the program runs this "else" section
        n=2
        f1=1
        f2=0
    #"n" is the current index value that program is calculating. n
    #will increase by 1 for every interation of the below while loop.
    #f1 is the previous fibonacci number in the sequence, while f2 is
    #the Fibonacci number before f1.
        while n <= idx:
           f = f2+f1
    #This while loop uses the bottom-up method of calculating the
    #Fibonacci number. f is the current Fibonacci number the program
    #is calculating.
           n = n+1
           f2 = f1
           f1 = f
    #Lines 33-35 set up the program for the next iteration of the while
    #loop. 
        return f
    #Once n becomes greater than idx, the while loop ends. fib(idx) will
    #return the final value of f.
    
again='null'
while again != 'q':
    #"again" is a variable that can only be in two states: 'null' or 'q'.
    #While "again" is in the 'null' state, it will repeat the loop. The user
    #will redefine the variable to the 'q' state when they are ready to quit
    #the program.
    if __name__ == '__main__':
    #This line makes this block only run when the script is executed in a 
    #standalone program. If the script is imported, it will not run.
        try:
            inputmessage = input ('Please enter an index: ')
            idx = int (inputmessage)
    #The program will try to convert the user input to an integer. If it
    #an error occurs, it will run the "except" section.
        except:
            print ('This a string! Please enter a nonnegative integer.')
            continue
    #The user will receive an error message, and the while loop starts again.
        else:
    #if the try statement succeeds, the else section runs
            if isinstance (idx, int) == False:
                print ('This is not an integer! Please enter a nonnegative integer.')
                continue
    #Lines 62-64 checks if the index is noninteger. If so, it sends an error
    #message, and the loop starts again.
            elif idx < 0:
                print ('This value is negative! Please enter a nonnegative integer.')
                continue
    #Lines 62-64 checks if the index is negative. If so, it sends an error
    #message, and the loop starts again.
            else:
                print ('The Fibonacci number at index', idx, 'is: ', fib (idx))
                again= input ('Press "Enter" to continue, type "q" to close the program: ')
    #If the index passes all previous tests, the program runs the fib (idx)
    #function, prints a message that displays this number, and prompts the
    #user to either continue or quit the program.