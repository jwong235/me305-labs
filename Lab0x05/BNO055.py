# -*- coding: utf-8 -*-
''' @file               BNO055.py
    @brief              Defines the BNO055 Class
    @details            Defines IMU functions
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               November 11, 2021
'''
from pyb import I2C
import utime

class BNO055:
    ''' @ brief         Interface with IMU
        @ details       Creates a list of functions for IMU
    '''
    def __init__(self, I2C_object):
        ''' @ brief             Constructor block for IMU
            @ details           Constructs an IMU object
            @param I2C_object   The I2C object
        '''
        
        self.I2C_object = I2C_object
        
    def set_operating_mode (self, op_mode):
        ''' @ brief             Sets new operating mode
            @ details           Takes an input parameter to set a new 
                                operating mode for the IMU
            @param op_mode      The desired operating mode of the IMU
        '''    
        
        self.I2C_object.mem_write(op_mode, 0x28, 0x3D)
        print(self.I2C_object.mem_read(1, 0x28, 0x3D))
            
    def calibration_status (self):
        ''' @ brief             Displays calibration status of IMU
            @ details           Returns four values. If all four values are 
                                3s, IMU is calibrated. Else, device is 
                                calibrating
        '''
                
        cal_bytes=self.I2C_object.mem_read(1, 0x28, 0x35)
        #print("Binary:", '{:#010b}'.format(cal_bytes[0]))

        cal_status = ( cal_bytes[0] & 0b11,
                      (cal_bytes[0] & 0b11 << 2) >> 2,
                      (cal_bytes[0] & 0b11 << 4) >> 4,
                      (cal_bytes[0] & 0b11 << 6) >> 6)
        
        #print("Values:", cal_status)
        return cal_status

    def retrieve_calibration_coef(self):
        ''' @ brief             Displays calibration coefficients
            @ details           Prints 22 calibration coefficients in a 
                                bytearray. User can save these values for 
                                future use.
        '''
        print (self.I2C_object.mem_read(22,0x28,0x55))
        
    def write_calibration_coef(self, cal_coef):
        ''' @ brief             Writes calibration coefficients
            @ details           User inputs a bytearray of 22 values and writes
                                calibration coefficients to the IMU
            @param cal_coef     bytearray of 22 calibration coefficient bytes
        '''        
        cal_bytes= bytearray(cal_coef)
        
    def read_euler(self):
        ''' @ brief             Reads euler angles
            @ details           Returns euler angles: yaw, pitch, and roll
                                of the IMU
        '''        
        
        self.eul_bytes = self.I2C_object.mem_read(6, 0x28, 0x1A)

        
        # First combine MSB and LSB for each value to get unsigned 16-bit integers
        self.yaw = self.eul_bytes[0] | self.eul_bytes[1] << 8  # EUL_Heading
        self.roll = self.eul_bytes[2] | self.eul_bytes[3] << 8  # EUL_Roll
        self.pitch = self.eul_bytes[4] | self.eul_bytes[5] << 8  # EUL_Pitch
        #print("Unsigned:", (self.yaw, self.roll, self.pitch))
        
        # Second, sign extend to get signed integers
        if self.yaw > 32767:
            self.yaw -= 65536
        if self.roll > 32767:
            self.roll -= 65536
        if self.pitch > 32767:
            self.pitch -= 65536
        #print("Signed:", (self.yaw, self.roll, self.pitch))
        
        # Third, scale to get proper units
        self.yaw /= 16
        self.roll /= 16
        self.pitch /= 16
        #print("Scaled:", (self.yaw, self.roll, self.pitch))
        #print('\n')
        return self.yaw, self.roll, self.pitch
        
    def read_angular_vel(self):
        ''' @ brief             Reads angular velocities
            @ details           Returns angular velocities: vel_x, vel_y, and 
                                vel_z of the IMU
        '''   
        
        self.vel_bytes = self.I2C_object.mem_read(6, 0x28, 0x14)
    
        # First combine MSB and LSB for each value to get unsigned 16-bit integers
        self.vel_x = self.vel_bytes[0] | self.vel_bytes[1] << 8  # EUL_Heading
        self.vel_y    = self.vel_bytes[2] | self.vel_bytes[3] << 8  # EUL_Roll
        self.vel_z   = self.vel_bytes[4] | self.vel_bytes[5] << 8  # EUL_Pitch
        #print("Unsigned:", (self.vel_x, self.vel_y, self.vel_z))
        
        # Second, sign extend to get signed integers
        if self.vel_x > 32767:
            self.vel_x -= 65536
        if self.vel_y > 32767:
            self.vel_y -= 65536
        if self.vel_z > 32767:
            self.vel_z -= 65536
        #print("Signed:", (self.vel_x, self.vel_y, self.vel_z))
        
        # Third, scale to get proper units
        self.vel_x /= 16
        self.vel_y /= 16
        self.vel_z /= 16
        #print("Scaled:", (self.vel_x, self.vel_y, self.vel_z))
        
        return self.vel_x, self.vel_y, self.vel_z
           
if __name__ == '__main__':
    i2c=I2C(1, I2C.MASTER)
    imu=BNO055(i2c)
    imu.set_operating_mode(12)
    state='S0_Check_state'
    
    while True:
        if state=='S0_Check_state':
            if imu.calibration_status()==(3, 3, 3, 3):
                print ('Device is calibrated.')
                state='S1_Print'
            else:
                print ("Calibration in progress")
                state='S2_IDLE'
        if state=='S1_Print':
            euler_data=imu.read_euler()
            print('------------------------')
            print('Yaw, Pitch, Roll (deg)')
            print (euler_data)
            #print('\n')
            
            #vel_data=imu.read_angular_vel()
            #print('Angular Velocity X, Y, Z')
            #print (vel_data)
            #print('\n')
            
            if imu.calibration_status()!=(3, 3, 3, 3):
                state='S0_Check_state'    
                
        if state=='S2_IDLE':
            print (imu.calibration_status())
            if imu.calibration_status()==(3, 3, 3, 3):
                state='S0_Check_state'
                
        #print (imu.calibration_status())
        utime.sleep(1)