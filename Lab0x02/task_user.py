''' @file               task_user.py
    @brief              A task for user interface
    @details            Allows user to input commmands to receive data
    @author             Jacob Wong, Kyle Hammer
    @date               October 21, 2021
'''

import utime, pyb

class Task_User():
    ''' @brief      User interface task for cooperative multitasking program.
        @details    Implements a finite state machine
    '''
    
    def __init__(self, name, period, enc_share, enc_delta, z_flag, dbg=False):
        ''' @brief              Constructs a user task.
            @details            The user task is implemented as a finite state
                                machine.
            @param name         The name of the task
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param enc_share    A shares.Share object used to hold the encoder
                                position.
            @param enc_delta    A shares.Share object used to hold the delta.
            @param z_flag       A shares.Share object used flag when z is pressed.
            @param dbg          A boolean flag used to enable or disable debug
                                messages printed over the VCP
        '''
        ## The name of the task
        self.name = name
        ## The period (in us) of the task
        self.period = period
        ## A shares.Share object representing encoder position
        self.enc_share = enc_share
        # A shares.Share object for delta
        self.enc_delta = enc_delta
        ## A flag that indicates when z button is pushed
        self.z_flag=z_flag
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The state to run on the next iteration of the finite state machine
        self.state = 'S0_INIT'
        ## The number of runs of the state machine
        self.runs = 0
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
        '''
        current_time = utime.ticks_us()
        #Runs only when self.next_time is met
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            
            if self.state == 'S0_INIT':
                # Instruction message
                print('Please read the following instructions:\n'
                      'Press p or P to display encoder position.\n'
                      'Press d or D to display current delta.\n'
                      'Press g or G to collect data.\n'
                      'Press z or Z to zero the position of the encoder.')
                self.transition_to('S1_IDLE')
                
            elif self.state == 'S1_IDLE':
                # Waits for character input from user
                if (self.ser.any()):
                    char_in = self.ser.read(1).decode()
                    
                    if(char_in == 'p' or char_in == 'P'):
                        self.state = 'S2_DISPLAY_POS'
                        
                    elif(char_in == 'd' or char_in == 'D'):
                        self.state = 'S3_DISPLAY_DELTA'
                        
                    elif(char_in == 'g' or char_in == 'G'):
                        self.state = 'S4_COLLECT_DATA'
                        self.starttimeS4=current_time
                        print('Collecting data for 30 seconds...'
                              'Press s or S to end data collection early')
                    
                    elif(char_in == 'z' or char_in == 'Z'):
                        self.state = 'S6_ZERO_POS'
                        self.z_flag.write(1)
                    else:
                        #if the input from user is not "P", "D", "G", or "Z"
                        #then sends an error message
                        print('Command \'{:}\' is invalid.'.format(char_in))
          
            elif self.state == 'S2_DISPLAY_POS':
                #prints current position from shares object
                print (self.enc_share.read())
                self.state = 'S1_IDLE'
                
            elif self.state == 'S3_DISPLAY_DELTA':
                #prints curent delta from shares object
                print (self.enc_delta.read())
                self.state = 'S1_IDLE'
                
            elif self.state == 'S4_COLLECT_DATA':
                #prints data at same time as collected
                print("{},{}".format(utime.ticks_diff(current_time,self.starttimeS4)/1000000,self.enc_share.read())) 
                if (self.ser.any()):
                    char_s = self.ser.read(1).decode()
                    #ends data collection when user presses "S"
                    if (char_s == 's' or char_s == 'S'):
                        self.state='S5_PRINT_DATA'
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_s))
                    #ends data collection when 30 seconds elapses
                elif utime.ticks_diff(current_time, self.starttimeS4) >= 30000000:
                        self.state='S5_PRINT_DATA'
                    
            elif self.state == 'S5_PRINT_DATA':
                print ('Data collection complete.')
                self.state= 'S1_IDLE'
                
            elif self.state == 'S6_ZERO_POS':
                #notifies that position has been zeroed (z-flag aleady set to 1)
                print ('Position has been zeroed')
                self.state = 'S1_IDLE'
    
            else:
                raise ValueError('Invalid State')
            #defines the next time task will run
            self.next_time = utime.ticks_add(self.next_time, self.period)
            #Increases run count
            self.runs += 1
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state