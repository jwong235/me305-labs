''' @file               Lab0x02.py
    @brief              Defines the main program
    @details            Establishes encoder objects and runs our tasks
    @author             Jacob Wong, Kyle Hammer
    @date               October 21, 2021
'''
# Import all needed modules
import time
import pyb
import encoder
import shares
import task_user
import task_encoder

def main():
    ''' @brief The main program
    '''
    # Establishing all share objects
    enc_pos=shares.Share(0)
    z_flag=shares.Share(0)
    enc_delta=shares.Share(0)
    Enc1=encoder.Encoder(4, pyb.Pin(pyb.Pin.board.PB6), pyb.Pin(pyb.Pin.board.PB7))
    #Enc2=encoder.Encoder(3, pyb.Pin(pyb.Pin.board.PC6),pyb.Pin(pyb.Pin.board.PC7))
    task1=task_encoder.Task_Enc('T1', 10_000, enc_pos, enc_delta, Enc1, z_flag)
    
    task2=task_user.Task_User('T2', 10_000, enc_pos, enc_delta, z_flag)
    
    # Pass Task_Enc and Task_User into ___
    task_list = [task1, task2]
    
    while(True):
        # Loops through the Tasks in task list
        try:
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
    pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

if __name__== '__main__':

    main()