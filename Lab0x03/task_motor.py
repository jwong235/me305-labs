
''' @file               task_motor.py
    @brief              A task for user interface
    @details            Allows user to input commmands to receive data
    @author             Jacob Wong, Kyle Hammer
    @date               November 3, 2021
'''
import DRV8847
import utime 
import pyb

class Task_Motor():
    ''' @brief      User interface task for cooperative multitasking program.
        @details    Implements a finite state machine
    '''
    
    def __init__(self, name, period, motor_drv, motor, c_flag, duty_cycle, duty_flag, dbg= False):
        ''' @brief              Constructs a user task.
            @details            The user task is implemented as a finite state
                                machine.
            @param name         The name of the task
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param enc_share    A shares.Share object used to hold the encoder
                                position.
            @param enc_delta    A shares.Share object used to hold the delta.
            @param z_flag       A shares.Share object used flag when z is pressed.
            @param dbg          A boolean flag used to enable or disable debug
                                messages printed over the VCP 
        '''
        ## The name of the duty cycle                        
        self.duty_cycle = duty_cycle
        ## The name of the task
        self.name = name
        ## The period (in us) of the task
        self.period = period
        ## The motor passed in
        self.motor = motor
        ## The c flag to clear faults
        self.c_flag = c_flag
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## Starts the fsm at the idle state
        self.state = 'S0_IDLE'
        ## Motor Driver Object
        self.motor_drv=motor_drv
        ## Duty cycle flag gets triggered whenever duty cycle is entered in task_user
        self.duty_flag=duty_flag
        ## Object for debugging
        self.dbg = dbg
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Contains three states: 
                        State 0: is an idle state that checks if a fault has occured 
                        and the c_flag or duty_flag has been triggered. 
                        State 1: updates the duty cycle by setting the duty and then reseting the duty flag.
                        State 2: clears the fault when c_flag is triggered. 
                        It then resets the c_flag and enables the motor driver.
        ''' 
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            

            
            if self.state == 'S0_IDLE':
                if self.motor_drv.fault_status == True:
                    print('Fault has occurred, please clear fault.')
                    self.motor_drv.fault_status= 'Idle'       
                elif self.c_flag.read()==1:
                    self.state='S2_CLEAR_FAULT'
                elif self.duty_flag.read()==1:
                    self.state='S1_UPDATE_DUTY_CYCLE'
            
            elif self.state == 'S1_UPDATE_DUTY_CYCLE':
                self.motor.set_duty(self.duty_cycle.read())
                print ('Set motor speed to:', self.duty_cycle.read())
                self.duty_flag.write(0)
                self.transition_to('S0_IDLE')
                
            elif self.state == 'S2_CLEAR_FAULT':
                self.motor_drv.fault_status = False
                self.motor_drv.enable()
                self.c_flag.write(0)
                self.state = 'S0_IDLE'
            
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1

    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state
