''' @file               task_user.py
    @brief              A task for user interface
    @details            Allows user to input commmands to receive data
    @author             Jacob Wong, Kyle Hammer
    @date               November 3, 2021
'''

import utime, pyb
import array

class Task_User():
    ''' @brief      User interface task for cooperative multitasking program.
        @details    Implements a finite state machine
    '''
    
    def __init__(self, name, period, motor_drv, c_flag, enc_share1, enc_delta1, z_flag1, duty_cycle1, duty_flag1, enc_share2, enc_delta2, z_flag2, duty_cycle2, duty_flag2, dbg=False):
        ''' @brief              Constructs a user task.
            @details            The user task is implemented as a finite state
                                machine.
            @param name         The name of the task
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param enc_share    A shares.Share object used to hold the encoder
                                position.
            @param enc_delta    A shares.Share object used to hold the delta.
            @param z_flag       A shares.Share object used flag when z is pressed.
            @param dbg          A boolean flag used to enable or disable debug
                                messages printed over the VCP
        '''
        ## The name of the task
        self.name = name
        ## The period (in us) of the task
        self.period = period
        ## The name of 
        self.motor_drv=motor_drv
        ## A shares.Share object that indicated when the c button is pushed
        self.c_flag = c_flag
        ## A shares.Share object representing encoder position
        self.enc_share1 = enc_share1
        ## A shares.Share object for delta of encoder 1
        self.enc_delta1 = enc_delta1
        ## A flag that indicates when z button is pushed
        self.z_flag1 = z_flag1
        ## A shares.Share object for the duty cycle of motor 1
        self.duty_cycle1 = duty_cycle1
        ## A flag to indicate when duty cycle 1 has been entered by user
        self.duty_flag1= duty_flag1
        ## A shares.Share object representing encoder position
        self.enc_share2 = enc_share2
        # A shares.Share object for delta of encoder 2
        self.enc_delta2 = enc_delta2
        ## A flag that indicates when Z button is pushed
        self.z_flag2 = z_flag2
        ## A shares.Share object for the duty cycle of motor 2
        self.duty_cycle2 = duty_cycle2
        ## A flag to indicate when duty cycle 2 has been entered by user
        self.duty_flag2=duty_flag2
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The state to run on the next iteration of the finite state machine
        self.state = 'S0_INIT'
        ## The number of runs of the state machine
        self.runs = 0
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## A variable that indicates whether motors are disabled or enabled
        self.enable_state=0
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
        '''
        current_time = utime.ticks_us()
        #Runs only when self.next_time is met
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            
            # State 0 displays instruction message
            if self.state == 'S0_INIT':
                print('---------------------------------------------------\n'
                      'Use the following commands:\n'
                      '---------------------------------------------------\n'
                      'z        Zero the position of encoder 1. \n'
                      'Z        Zero the position of encoder 2. \n'
                      'p        Print out the position of encoder 1.\n'
                      'P        Print out the position of encoder 2.\n'
                      'd        Print out the delta for encoder 1.\n'
                      'D        Print out the delta for encoder 2.\n'
                      'm        Enter a duty cycle for motor 1. \n'
                      'M        Enter a duty cycle for motor 2. \n'
                      'g        Collect data from encoder 1 for 30 seconds.\n'
                      'G        Collect data from encoder 2 for 30 seconds.\n'
                      'e or E   Enable or disable motors. \n'
                      'c or C   Clear fault.\n'
                      'h or H   Print this help message. \n'
                      'Ctrl-c   Terminate Program. \n'
                      '---------------------------------------------------')
                self.transition_to('S1_IDLE')
            
            # State 1 waits for character input from user
            elif self.state == 'S1_IDLE':
                if (self.ser.any()):
                    char_in = self.ser.read(1).decode()
                    
                    if(char_in == 'p'):
                        print ('Position of Encoder 1:')
                        self.state = 'S2_DISPLAY_POS1'
                        
                    elif(char_in == 'd'):
                        print ('Delta of Encoder 1:')
                        self.state = 'S3_DISPLAY_DELTA1'
                        
                    elif(char_in == 'g'):
                        print('Collecting data from Encoder 1 for 30 seconds...'
                              'Press s or S to end data collection early')
                        self.state = 'S4_COLLECT_DATA1'
                        self.starttimeS4=current_time
                    
                    elif(char_in == 'z'):
                        print ('Position of Encoder 1 has been zeroed')
                        self.state = 'S6_ZERO_POS1'
                                            
                    elif(char_in == 'P'):
                        print ('Position of Encoder 2:')
                        self.state = 'S7_DISPLAY_POS2'
                        
                    elif(char_in == 'D'):
                        self.state = 'S8_DISPLAY_DELTA2'
                        
                    elif(char_in == 'G'):
                        self.state = 'S9_COLLECT_DATA2'
                        self.starttimeS4=current_time
                        print('Collecting data for 30 seconds...'
                              'Press s or S to end data collection early')
                    
                    elif(char_in == 'Z'):
                        print ('Position of Encoder 2 has been zeroed')
                        self.state = 'S11_ZERO_POS2'
                        
                        
                    elif(char_in == 'm'):
                        self.state = 'S12_MOTOR1'
                        self.my_string=''
                        print('Enter Duty Cycle for Motor 1:')
                        
                    elif(char_in == 'M'):
                        self.state = 'S13_MOTOR2'
                        self.my_string=''
                        print('Enter Duty Cycle for Motor 2:')
                        
                    elif(char_in == 'c' or char_in == 'C'):
                        self.transition_to('S14_CLEAR_FAULT')
                        
                    elif(char_in == 'e' or char_in ==  'E'):
                        self.transition_to('S15_ENABLE_DISABLE')
                        
                    elif(char_in=='h' or char_in=='H'):
                        self.state='S0_INIT'
                        
                    else:
                        #if the input from user is not any of the previous letters
                        #then sends an error message
                        print('Command \'{:}\' is invalid.'.format(char_in))
          
            # State 2 prints current position of Encoder 1
            elif self.state == 'S2_DISPLAY_POS1':
                print (self.enc_share1.read())
                self.state = 'S1_IDLE'
            
            # State 3 prints current delta of Encoder 1
            elif self.state == 'S3_DISPLAY_DELTA1':
                print (self.enc_delta1.read())
                self.state = 'S1_IDLE'
            
            # State 4 collects and prints time and position data from Encoder 1
            elif self.state == 'S4_COLLECT_DATA1':
                print("{},{}".format(utime.ticks_diff(current_time,self.starttimeS4)/1000000,self.enc_share1.read())) 
                
                if (self.ser.any()):
                    char_s = self.ser.read(1).decode()
                    #ends data collection when user presses "S"
                    if (char_s == 's' or char_s == 'S'):
                        self.state='S5_PRINT_DATA1'
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_s))
                    #ends data collection when 30 seconds elapses
                elif utime.ticks_diff(current_time, self.starttimeS4) >= 30000000:
                        self.state='S5_PRINT_DATA1'
            
            # State 5 ends data collection
            elif self.state == 'S5_PRINT_DATA1':
                print ('Data collection complete.')
                self.state= 'S1_IDLE'
            
            # State 6 zeroes the position of Encoder 1
            elif self.state == 'S6_ZERO_POS1':
                self.z_flag1.write(1)
                self.state = 'S1_IDLE'
            
            # State 7 prints current position of Encoder 2
            elif self.state == 'S7_DISPLAY_POS2':
                #prints current position from shares object
                print (self.enc_share2.read())
                self.state = 'S1_IDLE'
             
            # State 8 prints current delta of Encoder 2
            elif self.state == 'S8_DISPLAY_DELTA2':
                print (self.enc_delta2.read())
                self.state = 'S1_IDLE'
            
            # State 9 collects and prints time and position data from Encoder 2    
            elif self.state == 'S9_COLLECT_DATA2':
                print("{},{}".format(utime.ticks_diff(current_time,self.starttimeS4)/1000000,self.enc_share2.read())) 
                if (self.ser.any()):
                    char_s = self.ser.read(1).decode()
                    #ends data collection when user presses "S"
                    if (char_s == 's' or char_s == 'S'):
                        self.state='S10_PRINT_DATA2'
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_s))
                    #ends data collection when 30 seconds elapses
                elif utime.ticks_diff(current_time, self.starttimeS4) >= 30000000:
                        self.state='S10_PRINT_DATA2'
            
            # State 10 ends data collection        
            elif self.state == 'S10_PRINT_DATA2':
                print ('Data collection complete.')
                self.state= 'S1_IDLE'
            
            # State 11 zeroes the position of Encoder 2
            elif self.state == 'S11_ZERO_POS2':
                self.z_flag2.write(1)
                self.state = 'S1_IDLE'
    
            # State 12 takes user input for duty cycle of Motor 1
            elif self.state == 'S12_MOTOR1':
                if (self.ser.any()):
                    char_in = self.ser.read(1).decode()
                    try:
                        input_val=int(char_in)
                        self.my_string+=str(input_val)
                        print(self.my_string)
                    except:
                        if (char_in== '-'):
                            if (self.my_string == ''):
                                self.my_string += char_in
                                print(self.my_string)
                        elif char_in == '\x7F':
                            self.my_string = self.my_string[:-1]
                            print('Backspace pressed')
                            print(self.my_string)
                        elif char_in == '\r':
                            #convert str to integer
                            self.my_string = int(self.my_string)
                            #print(self.my_string)
                            self.duty_cycle1.write(self.my_string)
                            self.duty_flag1.write(1)
                            self.state = 'S1_IDLE'
                                
            # State 13 takes user input for duty cycle of Motor 2   
            elif self.state == 'S13_MOTOR2':
                if (self.ser.any()):
                    char_in = self.ser.read(1).decode()
                    try:
                        input_val=int(char_in)
                        self.my_string+=str(input_val)
                        print(self.my_string)
                    except:
                        if (char_in== '-'):
                            if (self.my_string == ''):
                                self.my_string += char_in
                                print(self.my_string)
                        elif char_in == '\x7F':
                            self.my_string = self.my_string[:-1]
                            print('Backspace pressed')
                        elif char_in == '\r':
                            #convert str to integer
                            self.my_string = int(self.my_string)
                            self.duty_cycle2.write(self.my_string)
                            self.duty_flag2.write(1)
                            self.state = 'S1_IDLE'
            
            # State 14 clears the fault condition when user presses c or C
            elif self.state == 'S14_CLEAR_FAULT':   
                 self.c_flag.write(1)
                 print('Fault has been cleared')
                 self.state = 'S1_IDLE'
    
            # State 15 enables or disables motors
            elif self.state == 'S15_ENABLE_DISABLE':
                if self.enable_state==0:
                    self.motor_drv.enable()
                    self.enable_state=1
                    print ('Motors enabled')
                else:
                    self.motor_drv.disable()
                    self.enable_state=0
                    print ('Motors disabled')
                self.state='S1_IDLE'
                
            # If none of the previous states apply, then task returns an error message
            else:
                raise ValueError('Invalid State')
                
            #defines the next time task will run
            self.next_time = utime.ticks_add(self.next_time, self.period)
            #Increases run count
            self.runs += 1
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state