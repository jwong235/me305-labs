''' @file               encoder.py
    @brief              A driver for reading from Quadrature Encoders
    @details            Creates a class called Encoder that establishes
                        functions used for the encoder
    @author             Jacob Wong, Kyle Hammer
    @date               November 3, 2021
'''

import time
import pyb

class Encoder:
    ''' @brief          Interface with quadrature encoders
        @details        Creates a list of functions to be used
    '''

    def __init__(self, timnumb, pin1, pin2):
        ''' @brief          Constructs an encoder object
            @details        Constructor block to define input parameters
            @param timnumb  timer number
            @param pin1     pin1 of encoder
            @param pin2     pin2 of encoder
        '''
        ## Timer for encoder
        self.tim=pyb.Timer(timnumb, prescaler = 0, period = 65535)
        
        ## Timer channel 1
        self.ch1 = self.tim.channel(1, mode=pyb.Timer.ENC_AB, pin=pin1)
        ## Timer channel 2
        self.ch2 = self.tim.channel(2, mode=pyb.Timer.ENC_AB, pin=pin2)
        
        print('Creating encoder object')
        
        ## Current position of encoder
        self.position = 0
        ## Current position of timer counter
        self.current=0
        
    def update(self):
        ''' @brief      Updates encoder position and delta
            @details    Updates current position and delta while correcting
                        for overshoots and undershoots
        '''
        ## Previous position of timer counter
        self.past=self.current
        ## Current position of timer counter
        self.current=self.tim.counter()
        ## Change in position of timer counter
        self.delta=self.current-self.past
        
        if self.delta>=65535/2:
            self.delta-=65535
        if self.delta<=-65535/2:
            self.delta+=65535
        
        self.position+=self.delta
        
    def get_position(self):
        ''' @brief      Returns encoder position
            @details    Returns encoder position to task_user
            @return     The position of the encoder shaft
        '''
        return self.position

    def set_position(self, pos):
        ''' @brief              Sets encoder position
            @details            Allows for user to establish a new position of the encoder
            @param pos          The new position of the encoder
        '''
        self.position=pos
        print('Setting new position value')

    def get_delta(self):
        ''' @brief      Returns encoder delta
            @details    Sends delta when called
            @return     The change in position of the encoder shaft
                        between the two most recent updates
        '''
        return self.delta
