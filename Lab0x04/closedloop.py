''' @file               closedloop.py
    @brief              A closed loop driver
    @details            Creates a class called Encoder that establishes
                        functions used for the encoder
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               November 18, 2021
'''

class ClosedLoop:
    ''' @brief          class that establishes a closed loop
        @details        Creates a list of functions to be used for a closed
                        loop
    '''

    def __init__(self):
        ''' @brief      Constructs an closed loop object
            @details    Constructor block to define input parameters
        '''
        ## Gain
        self.kp=0
        ## Lower bound for PWM duty cycle
        self.L_lowerbound=0
        ## Upper bound for PWM duty cycle
        self.L_upperbound=100
        
    def update(self,speed_actual, speed_reference):
        ''' @brief                  Updates actuation value
            @details                Updates actuation value, L, and corrects L to be in 
                                    range of 0% to 100%
            @param speed_actual     Speed calculated from the encoder
            @param speed_reference  Desired speed from user input
            @return                 Actuation value, L
        '''
        ## Actuation value as a PWM duty cycle
        self.L=self.kp/12.2*(speed_reference-speed_actual)
        if self.L < self.L_lowerbound:
            self.L=self.L_lowerbound
        if self.L > self.L_upperbound:
            self.L=self.L_upperbound
        return (self.L)
        
    def get_Kp(self):
        ''' @brief      Returns Kp
            @details    Returns the proportional gain value, Kp, currently in use
            @return     proportional gain value, Kp
        '''
        return self.kp
    
    def set_Kp(self, Kp):
        ''' @brief              Sets Kp
            @details            Allows for user to establish a new Kp
            @param Kp           The new Kp
        '''
        self.kp=Kp