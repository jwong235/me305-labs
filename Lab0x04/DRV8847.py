''' @file DRV8847.py
    @brief              A file for creating motor drivers and motor functions
    @details            Creates classes called DRV8847 and Motor that establishes
                        functions used for the motor driver and motor
    @author             Jacob Wong, Kyle Hammer
    @date               November 3, 2021
'''

import pyb
import utime

class DRV8847:
    ''' @brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or moreobjects of the
                    class which can be used to perform motor
                    control.

                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''

    def __init__ (self, sleep_pin, fault_pin, timnum):
        ''' @brief      Initializes and returns a DRV8847 object.
            @details    Sets the sleep and fault pins. Sets the timer and frequency. 
            Also sets the fault interupt condition. 
        ''' 
        ## Shares object for Sleep pin, nSLEEP
        self.sleep_pin=sleep_pin #nSLEEP
        ## Shares object for Fault pin , nFAULT
        self.fault_pin=fault_pin #nFAULT
        ## Fault interupt
        self.FaultInt = pyb.ExtInt(self.fault_pin, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        ## Creates timer object 
        self.tim=pyb.Timer(timnum,freq=20000)
        ## Indicates if a fault is detected
        self.fault_status = False
    
    def enable (self):
        ''' @brief      Brings the DRV8847 out of sleep mode.
            @details    Brings out of sleep mode by setting the sleep pin to high. 
                        There is a short delay during which the interrupt service requests from the fault are ignored.
        '''
        self.FaultInt.disable()  #Disable fault interrupt
        self.sleep_pin.high()    #Re-enable the motor driver
        utime.sleep_us(25)       #Wait for the fault pin to return high
        self.FaultInt.enable()   #Re-enable the fault interrupt
        
    def disable (self):
        ''' @brief      Puts the DRV8847 in sleep mode.
            @details    Sets the sleep pin to low
        '''
        self.sleep_pin.low()
    
    def fault_cb (self, IRQ_src):
        ''' @brief      Callback function to run on fault condition.
            @param      IRQ_src The source of the interrupt request.
        '''
        self.sleep_pin.low()
        self.fault_status = True
    
    def motor (self, channelnum1 ,channelnum2, pin1, pin2):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @return     An object of class Motor
            @details    Takes in the channel two timer channels and the two motor pins.
        '''
        return Motor(channelnum1 ,channelnum2, pin1, pin2, self.tim)

class Motor:
    ''' @brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''

    def __init__ (self, channelnum1 ,channelnum2, pin1, pin2, tim):
        ''' @brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
        '''
        ## Timer of motor
        self.tim=tim
        ## Timer channel 1
        self.timerchannel1=self.tim.channel(channelnum1,pyb.Timer.PWM,pin=pin1)
        ## Timer channel 1
        self.timerchannel2=self.tim.channel(channelnum2,pyb.Timer.PWM,pin=pin2)
        
    
    def set_duty (self, duty):
        ''' @brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty A signed number holding the duty
                        cycle of the PWM signal sent to the motor
        '''
        if duty>=0:
            self.timerchannel1.pulse_width_percent(duty)
        else:
            self.timerchannel2.pulse_width_percent(abs(duty))
        
if __name__ == '__main__':
    ## motor driver object
    motor_drv = DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, 3)
    ## motor object 1
    motor_1 = motor_drv.motor(1,2,pyb.Pin.cpu.B4,pyb.Pin.cpu.B5)
     ## motor object 2
    motor_2 = motor_drv.motor(3,4,pyb.Pin.cpu.B0,pyb.Pin.cpu.B1)
    
    # Enable the motor driver
    motor_drv.disable()
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    motor_1.set_duty(30)
    motor_2.set_duty(80)