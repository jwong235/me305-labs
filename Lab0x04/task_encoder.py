''' @file               task_encoder.py
    @brief              A task for encoder
    @details            Constantly updates position of encoder and can also 
                        zero the position of the encoder when prompted.
    @author             Jacob Wong, Kyle Hammer
    @date               November 3, 2021
'''
import time
import encoder
import pyb
import utime

class Task_Enc():
    ''' @brief      Encoder task for cooperative multitasking example.
        @details    Implements a finite state machine
    '''
    
    def __init__(self, name, period, enc_share, enc_delta, enc_object, z_flag, dbg=False):
        ''' @brief              Constructs an encoder task.
            @details            The encoder task is implemented as a finite state
                                machine.
            @param name         The name of the task
            @param period       The period, in microseconds, between runs of 
                                the task.
            @param enc_share    A shares.Share object used to hold the encoder
                                position.
            @param enc_delta    A shares.Share object used to hold delta
            @param enc_object   the encoder object
            @param z_flag       A flag that indicates when z or Z is pressed.
            @param dbg          A boolean flag used to enable or disable debug
                                messages printed over the VCP
        '''
        
        ## The name of the task
        self.name = name
        ## Ther period (in us) of the task
        self.period = period
        ## A shares.Share object representing encoder position
        self.enc_share = enc_share
        ## A shares.Share object used to hold delta
        self.enc_delta = enc_delta
        ## the encoder object
        self.enc_object = enc_object   
        ## A flag that indicates when z button is pushed
        self.z_flag= z_flag
        ## A flag indicating if debugging print messages display
        self.dbg = dbg
        
        ## Creates a timer
        self.tim2 = pyb.Timer(2, freq = 20000)
        ## The timer channel used for updating encoder position.
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=pyb.Pin.cpu.A5,
                                  pulse_width_percent=0)
        
        ## The state to run on the next iteration of the finite state machine
        self.state = 'S0_UPDATE_POSITION'
        ## The number of runs of the state machine
        self.runs = 0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
            @details    Contains two states: 
                        State 0: updates position of encoder
                        State 1: zeros position of encoder
        '''  
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            
            # Conditional statement to detect if z flag has been raised
            if self.z_flag.read()==1:
                self.state='S1_ZERO_POSITION'
            else:
                self.state='S0_UPDATE_POSITION'
            
            # This state updates the position the encoder
            if self.state == 'S0_UPDATE_POSITION':
                self.enc_object.update()
                self.enc_share.write(self.enc_object.get_position())
                self.enc_delta.write(self.enc_object.get_delta())
            
            # This state sets the position of an encoder to zero and then goes to the idle state    
            elif self.state == 'S1_ZERO_POSITION':
                self.enc_object.set_position(0)
                self.z_flag.write(0)
                self.transition_to('S0_UPDATE_POSITION')
                
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state

