''' @file               Lab0x02.py
    @brief              Defines the main program
    @details            Establishes encoder objects and runs our tasks
    @author             Jacob Wong, Kyle Hammer
    @date               November 3, 2021
'''
# Import all needed modules
import time
import pyb
import encoder
import shares
import task_user
import task_encoder
import task_motor
import DRV8847

def main():
    ''' @brief      The main program
        @details    Establishes all the share objects for both encoders. 
                    Two encoder tasks are established. Two motor tasks are established. User task is established.
                    The while loop goes through the tasks in the tasks list.
     '''
    # Establishing all share objects
    enc_pos1=shares.Share(0)
    z_flag1=shares.Share(0)
    c_flag = shares.Share(0)
    enc_delta1=shares.Share(0)
    duty_cycle1 = shares.Share(0)
    duty_flag1=shares.Share(0)
    
    # Share objects for encoder 2
    enc_pos2=shares.Share(0)
    z_flag2=shares.Share(0)
    enc_delta2=shares.Share(0)
    duty_cycle2 = shares.Share(0)
    duty_flag2=shares.Share(0)
    
    #Creates motor objects and motor driver object
    #Sets pins and timer channels for both motors and the motor driver
    motor_drv = DRV8847.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2, 3)
    motor_1 = motor_drv.motor(1,2,pyb.Pin.cpu.B4,pyb.Pin.cpu.B5)
    motor_2 = motor_drv.motor(3,4,pyb.Pin.cpu.B0,pyb.Pin.cpu.B1)
    
    #Creates encoder ojects
    #Sets pins and timer channels for both encoder objects
    Enc1=encoder.Encoder(4, pyb.Pin(pyb.Pin.board.PB6), pyb.Pin(pyb.Pin.board.PB7))
    Enc2=encoder.Encoder(8, pyb.Pin(pyb.Pin.board.PC6), pyb.Pin(pyb.Pin.board.PC7))
    
    #Creates tasks for Encoder 1 and Encoder 2
    task1=task_encoder.Task_Enc('T1', 10_000, enc_pos1, enc_delta1, Enc1, z_flag1)
    task2=task_encoder.Task_Enc('T1', 10_000, enc_pos2, enc_delta2, Enc2, z_flag2)
    
    #Creates tasks for Motor 1 and motor 2
    task3=task_motor.Task_Motor('T1', 10_000, motor_drv, motor_1, c_flag, duty_cycle1, duty_flag1)           #Enc1? 
    task4=task_motor.Task_Motor('T1', 10_000, motor_drv, motor_2, c_flag, duty_cycle2 ,duty_flag2)           #Enc2?
    
    #Creates user task
    task5=task_user.Task_User('T2', 10_000, motor_drv, c_flag, enc_pos1, enc_delta1, z_flag1, duty_cycle1, duty_flag1, enc_pos2, enc_delta2, z_flag2, duty_cycle2,duty_flag2)
    
    # Pass Task_Enc, Task_Motor and Task_User into task_list to be run in while loop
    task_list = [task1, task2, task3, task4, task5]
    
    while(True):
        # Loops through the Tasks in task list
        try:
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
    pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

if __name__== '__main__':

    main()