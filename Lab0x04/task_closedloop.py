''' @file               task_closedloop.py
    @brief              A task operating a closed loop
    @details            Creates a class called task_ClosedLoop that establishes
                        a finite state machine for a closed feedback loop
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               November 18, 2021
'''

import utime

class Task_ClosedLoop():
    ''' @brief      Closed loop task
        @details    Implements a finite state machine for closed feedback loop.
    '''
    
    def __init__(self, name, period, CL_object, kp_flag, kp, ref_speed_flag, ref_speed, enc_speed, L_share, L_flag, dbg=False):
        ''' @brief                  Constructs an closed loop task.
            @details                The closed loop task is implemented as a finite state
                                    machine.
            @param name             The name of the task
            @param period           The period, in microseconds, between runs of 
                                    the task.
            @param CL_object        A closed loop object
            @param kp_flag          A flag raised when user inputs kp
            @param kp               A shares object holding value of Kp
            @param ref_speed_flag   A flag raised when user inputs a reference speed
            @param ref_speed        A shares object holding value of reference speed
            @param enc_speed        A shares object holding the speed of motor read by the encoder
            @param L_share          A shares object holding the calculated actuation value
            @param L_flag           A flag raised when an L is calculated
            @param dbg              A boolean flag used to enable or disable debug
                                    messages printed over the VCP
        '''
        ## Task Name
        self.name=name
        ## Task period
        self.period=period
        ## Closed Loop object from driver
        self.CL_object=CL_object
        ## Flag that indicates when user inputs a Kp
        self.kp_flag=kp_flag
        ## shares.Share object holding value of Kp
        self.kp=kp
        ## Flag that indicates when user inputs a reference speed
        self.ref_speed_flag = ref_speed_flag
        ## shares.Share object that holds reference speed
        self.ref_speed = ref_speed
        ## shares.Share object that holds encoder speed
        self.enc_speed = enc_speed
        ## shares.Share object that holds actuation value, L
        self.L_share = L_share
        ## A flag that indicates when a value of L is inputted
        self.L_flag=L_flag
        ## A debug flag
        self.dbg=dbg
        ## The current state of the FSM
        self.state='S0_UPDATE_CL'
        ## The next time the program will run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## Counts number of runs
        self.runs = 0
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
            @details    Contains three states: 
                        State 0: updates actuation value
                        State 1: prints current Kp value
                        State 2: Sets a new Kp value from user input
        '''  
        ## Current time of the program
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.kp_flag.read()==1:
                self.transition_to('S2_SET_KP')
            # This state updates the position the encoder
            if self.state == 'S0_UPDATE_CL':
                ## The actuation value 
                self.L = self.CL_object.update(self.enc_speed.read(), self.ref_speed.read())
                self.L_share.write(self.L)
                self.L_flag.write(1)
                
            elif self.state == 'S1_GET_KP':
                print(self.CL_object.get_Kp())
                self.state = 'S0_UPDATE_CL'
                
            elif self.state == 'S2_SET_KP':
                self.CL_object.set_Kp(self.kp.read())  #1 input parameter here
                self.kp_flag.write(0)
                self.state = 'S0_UPDATE_CL'
                
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state