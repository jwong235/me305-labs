''' @file               main.py
    @brief              Defines the main program
    @details            Establishes encoder objects and runs our tasks
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

#Import all needed modules
import pyb
from pyb import Pin
from pyb import I2C
import shares
import task_touch1
import touch1
import motor_driver1
import BNO055_1
import closedloop1
import task_closedloop1
import task_motor1
import task_imu1
import task_user1

def main():
    ''' @brief      The main program
        @details    Creates all the share objects.
                    Touch task is established. 
                    IMU task is established.
                    Two closed loop tasks are established.
                    Two motor tasks are established. 
                    User task is established.
                    
     '''
    ## shares.Share object for the x position of the ball
    x=shares.Share(0)
    ## shares.Share object for the y position of the ball
    y=shares.Share(0)
    ## shares.Share object for the z position of the ball
    z=shares.Share(0)
    ## shares.Share object for the x velocity of the ball
    xd=shares.Share(0)
    ## shares.Share object for the y velocity of the ball
    yd=shares.Share(0)
    ## shares.Share object for the angular position about the x axis of the ball
    th_x = shares.Share(0)
    ## shares.Share object for the angular position about the y axis of the ball
    th_y = shares.Share(0)
    ## shares.Share object for the angular velocity about the x axis of the ball
    thd_x = shares.Share(0)
    ## shares.Share object for the angular position about the y axis of the ball
    thd_y = shares.Share(0)
    ## shares.Share object that indicates that the user has started the balancing program
    balancing_flag=shares.Share(0)
    ## shares.Share object for holding the calculated actuation value for the motor in the x direction
    L_share1=shares.Share(0)
    ## shares.Share object for holding the calculated actuation value for the motor in the y direction
    L_share2=shares.Share(0)
    ## The i2c object
    i2c=I2C(1, I2C.MASTER)
    ## The imu object
    imu=BNO055_1.BNO055(i2c)
    ## Sets the imu operating mode to 12
    imu.set_operating_mode(12)
    
    ## Sets timer object for the motors
    timer = pyb.Timer(3,freq=20000)
    ## Motor object that sets pyb pins and timer for motor 1
    motor_1 = motor_driver1.Motor(1,2,pyb.Pin.cpu.B4,pyb.Pin.cpu.B5, timer)
    ## Motor object that sets pyb pins and timer for motor 2
    motor_2 = motor_driver1.Motor(3,4,pyb.Pin.cpu.B0,pyb.Pin.cpu.B1, timer)
    
    ## Touch object that sets the pins for the touch panel
    touch_object = touch1.Panel(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0, 100, 176) 
    
    ## Task_Touch controls and operates the touch panel
    task1 = task_touch1.Task_Touch('T1', 10_000, touch_object, x, y, z,xd,yd)
    
    ## Task_IMU updates data from the IMU attached to the top of the platform and updates share objects for the angular position and angular velocity
    task2 = task_imu1.Task_IMU('T1', 10_000, imu, th_x, th_y, thd_x, thd_y)
    
    ## Closed loop object created from class ClosedLoop
    CL_objectX = closedloop1.ClosedLoop(x,th_x, xd,thd_x)
    CL_objectY = closedloop1.ClosedLoop(y,th_y, yd,thd_y)
    
    ## Task_ClosedLoop 
    task3 = task_closedloop1.Task_ClosedLoop('T3', 10_000, CL_objectX, L_share1)
    task4 = task_closedloop1.Task_ClosedLoop('T4', 10_000, CL_objectY, L_share2)
    
    ## Task_Motor 
    task5 = task_motor1.Task_Motor('T5', 10_000, motor_1, L_share1, z, balancing_flag)
    task6 = task_motor1.Task_Motor('T6', 10_000, motor_2, L_share2, z, balancing_flag)
    
    ## Task_User uses a FSM to take user input 
    task7 = task_user1.Task_User('T7',10_000, balancing_flag, x, y, imu)
    
    ## List of all tasks to run 
    task_list = [task1, task2, task3, task4, task5, task6, task7]
    
    while(True):
        try:
            for task in task_list:
                task.run()
                
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
    pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

if __name__== '__main__':

    main()