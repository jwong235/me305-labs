''' @file               task_imu1.py
    @brief              A task updating the IMU
    @details            Writes data from the IMU to several shares variables to read elsewhere
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

import utime
import time
import os

class Task_IMU():
    ''' @brief      Class of IMU task
        @details    Implements a finite state machine that writes the euler data from the IMU to shares variables
    '''
    
    def __init__(self, name, period, imu, th_x, th_y, thd_x, thd_y, dbg=False):
        ''' @brief                  Constructs an IMU task
            @details                Constructor block containing all of our input parameters for class Task_IMU
            @param name             The name of the task
            @param period           The period, in microseconds, between runs of 
                                    the task. 
            @param imu              The imu object
            @param th_x             Angular position object about the x axis 
            @param th_y             Angular position object about the y axis
            @param thd_x            Angular velocity object about the x axis
            @param thd_y            Angular velocity object about the y axis
            @param dbg              A boolean flag used to enable or disable debug
                                    messages printed over the VCP
        '''
        ## The period, in microseconds, between runs of the task.
        self.period=period
        ## The imu object
        self.imu = imu
        ## Angular position object about the x axis
        self.th_x = th_x
        ## Angular position object about the y axis
        self.th_y = th_y
        ## Angular velocity object about the x axis
        self.thd_x = thd_x
        ## Angular velocity object about the y axis
        self.thd_y = thd_y
        ## The state to run on the next iteration of the finite state machine
        self.state = 'S0_UPDATE'
        ## Used as an intermediate value defines the next time the program will run
        self.next_time = 0
        ## The number of runs of the state machine
        self.runs = 0
        ## Debug flag
        self.dbg = dbg
        
        filename="IMU_cal_coeffs.txt"
        if filename in os.listdir():
            with open(filename, 'r') as f:
                cal_data_string=f.readline()
                cal_values=[float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                self.imu.write_calibration_coef(cal_values)
        else:
            print ("IMU Calibration in Progress")
            with open(filename, 'w') as f:
                while self.imu.calibration_status()!=(3, 3, 3, 3):
                    print ('IMU Calibration Status:', self.imu.calibration_status())
                    time.sleep(1)
                f.write(self.imu.retrieve_calibration_coef())
        
    def run(self):
            ''' @brief    Runs one iteration of the FSM
                @details  Reads euler data from the IMU and writes to the corresponding objects   
            '''
            ## The current time
            current_time = utime.ticks_us()
            if (utime.ticks_diff(current_time, self.next_time) >= 0):
                if self.state == 'S0_UPDATE': 
                    (self.yaw,self.pitch,self.roll)=self.imu.read_euler()
                    (self.avel_x,self.avel_y,self.avel_z)=self.imu.read_angular_vel()
                    self.th_x.write(self.pitch)
                    self.th_y.write(self.roll) 
                    self.thd_x.write(self.avel_x)
                    self.thd_y.write(self.avel_y)
                else:
                    raise ValueError('Invalid State')
                
                self.next_time = utime.ticks_add(self.next_time, self.period)
                self.runs += 1
        
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state

       