''' @file               task_motor1.py
    @brief              A task for operating the motors
    @details            Creates a class called Task_Motor that establishes
                        a finite state machine for a motor task
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

import utime

class Task_Motor():
    ''' @brief      Motor task
        @details    Implements a finite state machine for a motor task.
    '''
    
    def __init__(self, name, period, Motor, L_share, z, balancing_flag, dbg=False):
        ''' @brief                  Constructs an motor task.
            @details                Constructor block containing all of our input parameters for class Task_Motor
            @param name             The name of the task
            @param period           The period, in microseconds, between runs of 
                                    the task.
            @param Motor            A motor object 
            @param L_share          A shares object holding the calculated actuation value
            @param z                Object for the z position of the ball that 
                                    reads true if there is contact on the platform and 
                                    false if there is no contact 
            @param balancing_flag   Flag that indicates that the user has started the balancing program 
            @param dbg              A boolean flag used to enable or disable debug
                                    messages printed over the VCP
        '''
        ## The name of the task
        self.name=name
        ## The period, in microseconds, between runs of the task
        self.period=period
        ## A motor object 
        self.Motor=Motor 
        ## A shares object holding the calculated actuation value
        self.L_share = L_share
        ## Object for the z position of the ball that reads true if there is contact on the platform and false if there is no contact 
        self.z=z
        ## Flag that indicates that the user has started the balancing program
        self.balancing_flag=balancing_flag
        ## Debug flag
        self.dbg=dbg
        ## The state to run on the next iteration of the finite state machine
        self.state='S0_IDLE'
        ## Used as an intermediate value defines the next time the program will run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The number of runs of the state machine
        self.runs = 0
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
            @details    Contains two states: 
                        State 0: Idle state
                        State 1: Updates the duty cycle of the motor when user 
                        starts the balancing program and there is contact on the platform
        '''  
        ## The current time
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.balancing_flag.read()==0:
                self.state='S0_IDLE'
            else:
                self.state= 'S1_UPDATE'
                
            if self.state == 'S1_UPDATE':
                #print(self.L_share.read())
                if self.z.read()==False:
                    self.Motor.set_duty(0)
                    print('false')
                elif self.z.read()==True:
                    print(self.L_share.read())
                    self.Motor.set_duty(self.L_share.read())
                    #print('true')
                else:
                    raise ValueError('Problem setting duty cycle.')
            elif self.state=='S0_IDLE':
                pass
            else:
                raise ValueError('Invalid State in Task Motor')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state