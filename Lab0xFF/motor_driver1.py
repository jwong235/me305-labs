
''' @file               motor_driver1.py
    @brief              A file for creating motor objects and motor functions
    @details            Creates a class called Motor that establishes
                        functions used for the motor
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

import pyb

class Motor:
    ''' @brief      A motor class to create motor objects
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''

    def __init__ (self, channelnum1 ,channelnum2, pin1, pin2, tim):
        ''' @brief              Constructs a motor object
            @details            Objects of this class are created in main.py
            @param channelnum1  First channel number of motor
            @param channelnum2  Second channel number of motor
            @param pin1         First motor pin
            @param pin2         Second motor pin
            @param tim          Timer object
        '''
        ## Timer object for motor
        self.tim=tim
        ## The first timer channel of motor
        self.timerchannel1=self.tim.channel(channelnum1,pyb.Timer.PWM,pin=pin1)
        ## The second timer channel of motor
        self.timerchannel2=self.tim.channel(channelnum2,pyb.Timer.PWM,pin=pin2)
        
    
    def set_duty (self, duty):
        ''' @brief              Set the PWM duty cycle for the motor channel.
            @details            This method sets the duty cycle of the motor to 
                                the given PWM percentage level. Positive values
                                cause revolutions in one direction, negative 
                                values in the opposite direction.
            @param      duty    A signed value holding the duty
                                cycle of the PWM signal sent to the motor
        '''
        
        if duty>=0:
            self.timerchannel1.pulse_width_percent(duty)
            self.timerchannel2.pulse_width_percent(0)
        else:
            self.timerchannel2.pulse_width_percent(-duty)
            self.timerchannel1.pulse_width_percent(0)
        

    