''' @file               touch1.py
    @brief              A driver class for the touch panel
    @details            Creates a class to retrieve data from the touch panel
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''
from pyb import Pin
from pyb import ADC
import utime

class Panel:
    ''' @brief      A class for the touch panel
        @details    Retrieves data from the touch panel to calculate the x, y , 
                    and z position of the ball
    '''
    def __init__(self, xp, xm, yp, ym, width, length):
        ''' @brief          Constructs a panel object
            @details        Constructor block that establishes input parameters 
                            for a Panel object
            @param  xp      A pin representing xp of the touch panel
            @param  xm      A pin representing xm of the touch panel
            @param  yp      A pin representing yp of the touch panel
            @param  ym      A pin representing ym of the touch panel
            @param  width   The width of the touch panel
            @param  length  the length of the touch panel
        '''
        ## A pin representing xp of the touch panel
        self.xp = xp
        ## A pin representing xm of the touch panel
        self.xm = xm
        ## A pin representing yp of the touch panel
        self.yp = yp
        ## A pin representing ym of the touch panel
        self.ym = ym
        ## The width of the touch panel
        self.width = width
        ## The length of the touch panel
        self.length = length
        ## The x position of the center of the touch panel
        self.xc = length/2
        ## The y position of the center of the touch panel
        self.yc = width/2
        
    
    def x_scan(self):
        ''' @brief      Finds the x position of the ball
            @details    Takes a ADC reading from ym and converts the raw data 
                        to the x position in mm
            @return     x position of ball in mm
        '''
        ## Pin object for xm configured as output
        self.x_scan_xm = Pin(self.xm, Pin.OUT_PP)
        ## Pin object for xp configured as output
        self.x_scan_xp = Pin(self.xp, Pin.OUT_PP)
        ## ADC object for ym
        self.x_scan_ym = ADC(self.ym)
        ## Pin object for yp configured as input
        self.x_scan_yp = Pin(self.yp, Pin.IN)
        
        self.x_scan_xp.high()
        self.x_scan_xm.low()
        
        return (self.x_scan_ym.read()-212)/3500*self.length - self.xc -2.3
        
    def y_scan(self):
        ''' @brief      Finds the y position of the ball
            @details    Takes a ADC reading from xm and converts the raw data 
                        to the y position in mm
            @return     y position of ball in mm
        '''
        ## Pin object for ym configured as output
        self.y_scan_ym = Pin(self.ym, Pin.OUT_PP)
        ## Pin object for yp configured as output
        self.y_scan_yp = Pin(self.yp, Pin.OUT_PP)
        ## ADC object for xm
        self.y_scan_xm = ADC(self.xm)
        ## Pin object for xp configured as input
        self.y_scan_xp = Pin(self.xp, Pin.IN)
        
        self.y_scan_yp.high()
        self.y_scan_ym.low()
        return (self.y_scan_xm.read()-380)/3250*self.width - self.yc +2

    def z_scan(self):
        ''' @brief      Determine if there is contact with touch panel
            @details    Takes a ADC reading from ym in order to return a True 
                        or False if there is contact with the touch panel
            @return     True or False depending on if there is contact with the ball
        '''
        ## Pin object for yp configured as output
        self.z_scan_yp = Pin(self.yp, Pin.OUT_PP)
        ## Pin object for xm configured as output
        self.z_scan_xm = Pin(self.xm, Pin.OUT_PP)
        ## Pin object for xp configured as input
        self.z_scan_xp = Pin(self.xp, Pin.IN)
        ## ADC object for xm
        self.z_scan_ym = ADC(self.ym)
        
        self.z_scan_yp.high()
        self.z_scan_xm.low()
        
        if self.z_scan_ym.read() < 4000: 
            self.z_scan_result = True
            
        else:
            self.z_scan_result = False
        return self.z_scan_result
    
    def xyz_scan(self):
        ''' @brief      Finds the x, y, z position of the ball
            @details    A combination of the x_scan, y_scan, and z_scan functions
            @return     (x, y, z) position of ball
        '''
        self.x_scan_xm = Pin(self.xm, Pin.OUT_PP)
        self.x_scan_xp = Pin(self.xp, Pin.OUT_PP)
        self.x_scan_ym = ADC(self.ym)
        self.x_scan_yp = Pin(self.yp, Pin.IN)
        
        self.x_scan_xp.high()
        self.x_scan_xm.low()
        
        self.y_scan_ym = Pin(self.ym, Pin.OUT_PP)
        self.y_scan_yp = Pin(self.yp, Pin.OUT_PP)
        self.y_scan_xm = ADC(self.xm)
        self.y_scan_xp = Pin(self.xp, Pin.IN)
        
        self.y_scan_yp.high()
        self.y_scan_ym.low()
        
        self.z_scan_yp = Pin(self.yp, Pin.OUT_PP)
        self.z_scan_xm = Pin(self.xm, Pin.OUT_PP)
        self.z_scan_xp = Pin(self.xp, Pin.IN)
        self.z_scan_ym = ADC(self.ym)
        
        self.z_scan_yp.high()
        self.z_scan_xm.low()
        
        if self.z_scan_ym.read() < 4000: 
            self.z_scan_result = True
            
        else:
            self.z_scan_result = False
        return (self.x_scan_ym.read(), self.y_scan_xm.read(), self.z_scan_result)
    
    def get_scan(self):
        ''' @brief      Returns (x,y,z) of ball
            @return     Tuple of (x,y,z) of ball
        '''
        ## The x position of the ball, mm
        x = self.x_scan()
        ## The y position of the ball, mm
        y = self.y_scan()
        ## Value that is True or False depending on if there is contact with touch panel
        z = self.z_scan()
        return (x, y, z)
    
if __name__== '__main__':
    touch_panel = Panel(Pin.cpu.A7, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A0, 100, 176)
    #while True:
        #print(touch_panel.get_scan())
    current_time = utime.ticks_us()
    starttime=current_time
    runs = 0
    while runs <100:
        touch_panel.get_scan()
        runs+=1
    current_time = utime.ticks_us()
    timeelapsed = current_time - starttime
    print (timeelapsed)

    
