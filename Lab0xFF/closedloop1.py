''' @file               closedloop1.py
    @brief              A closed loop driver
    @details            Creates a class called ClosedLoop that establishes a 
                        closed feedback loop to find the PWM% for the motors
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

from ulab import numpy as np
import math

class ClosedLoop:
    ''' @brief          Class that creates a closed loop object
        @details        Creates a list of functions to be used for a closed
                        loop
    '''

    def __init__(self, x,th_y, xd,thd_y):
        ''' @brief          Constructs a closed loop object
            @details        Constructor block to define input parameters
            @param x        shares.Share object for position of ball
            @param th_x     shares.Share object for angular position of ball
            @param xd       shares.Share object for velocity of ball
            @param thd_x    shares.Share object for angular velocity of ball
        '''
        ## shares.Share object for position of ball
        self.x=x
        ## shares.Share object for angular position of ball
        self.th_y=th_y
        ## shares.Share object for velocity of ball
        self.xd=xd 
        ## shares.Share object for angular velocity of ball
        self.thd_y=thd_y
        ## The state vector, [x, th_x, xd, thd_x]
        self.x_state=np.array([self.x.read(),self.th_y.read()/180*math.pi, self.xd.read(),self.thd_y.read()/180*math.pi])
        ## Gains that when multiplied with x_state, we find the torque acting on the plate.
        self.k= np.array([0.3, 0.2, 0.05, 0.02])
        ## Lowerbound of actuation value
        self.L_lowerbound=-70
        ## Upperbound of actuation value
        self.L_upperbound=70
        
    def update(self):
        ''' @brief                  Updates actuation value
            @details                Updates actuation value, L, and adjusts the 
                                    value to be within the lower and upperbounds.
            @return                 Actuation value, L
        '''
        ## Internal Resistance of motor, ohms
        self.R=2.21 
        ## Torque Constant, N-m/A
        self.kt=13.8 
        ## Torque on platform, N-mm
        self.Tx=0.3*self.x.read()+0.2*self.th_y.read()/180*math.pi+0.05*self.xd.read()+0.02*self.thd_y.read()/180*math.pi
        ## Actuation value that sets PWM% for motors
        self.L= (100*self.R)/(4*self.kt*12)*self.Tx
        if self.L < self.L_lowerbound:
            self.L=self.L_lowerbound
        if self.L > self.L_upperbound:
            self.L=self.L_upperbound
        return (self.L)
        
