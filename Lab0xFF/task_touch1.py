''' @file               task_touch1.py
    @brief              A task operating a Touch panel interface
    @details            Responsible for interfacing with the resistive touch panel 
                        and using an object of the touch panel driver class. 
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

import utime

class Task_Touch():
    ''' @brief      Touch task
        @details    Implements a finite state machine for a touch task.
    '''
    
    def __init__(self, name, period, touch_object, x, y, z,xd, yd, dbg=False):
        ''' @brief                  Constructs an closed loop task.
            @details                Constructor block containing all of our input parameters for class Task_Touch
            @param name             The name of the task
            @param period           The period, in microseconds, between runs of 
                                    the task.
            @param touch_object     The touch pannel object made from class Panel
            @param x                Object for the x position of the ball 
            @param y                Object for the y position of the ball 
            @param z                Object for the z position of the ball that 
                                    reads true if there is contact on the platform and 
                                    false if there is no contact 
            @param xd               Object for the velocity of the ball in the x direction 
            @param yd               Object for the velocity of the ball in the y direction 
            @param dbg              A boolean flag used to enable or disable debug
                                    messages printed over the VCP
        '''
        ## The name of the task
        self.name=name
        ## The period, in microseconds, between runs of the task.
        self.period=period
        ## The touch pannel object made from class Panel
        self.touch_object = touch_object
        ## Object for the x position of the ball 
        self.x = x
        ## Object for the y position of the ball 
        self.y = y
        ## Object for the z position of the ball that reads true if there is contact on the platform and false if there is no contact 
        self.z = z
        ## Object for the velocity of the ball in the x direction 
        self.xd = xd
        ## Object for the velocity of the ball in the y direction 
        self.yd = yd
        ## Debug flag
        self.dbg=dbg
        ## The state to run on the next iteration of the finite state machine 
        self.state='S0_UPDATE'
        ## Used as an intermediate value defines the next time the program will run
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The number of runs of the state machine
        self.runs = 0
        ## Variable storing previous time step
        self.hold_time=0
        ## x position from the previous time step
        self.hold_x=0
        ## y position from the previous time step
        self.hold_y=0
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Continuously updates the x,y,z,xd,yd objects to track position and velocity of the ball  
        '''
        ## Current time
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == 'S0_UPDATE':
                self.time=utime.ticks_diff(current_time,self.hold_time)/1000000
                self.hold_time=current_time
                self.currentx=self.touch_object.x_scan()
                self.currenty=self.touch_object.y_scan()
                self.x.write(self.currentx)
                self.y.write(self.currenty)   
                self.z.write(self.touch_object.z_scan())
                self.xd.write((self.currentx-self.hold_x)/self.time)
                self.yd.write((self.currenty-self.hold_y)/self.time)
                self.hold_x=self.currentx
                self.hold_y=self.currenty
                
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state

