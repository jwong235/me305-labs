''' @file               task_user1.py
    @brief              A task for user interface
    @details            Allows user to input commmands to collect data, calibrate, balance
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

import utime, pyb

class Task_User():
    ''' @brief      User interface task for cooperative multitasking program.
        @details    Implements a finite state machine for a user task.
    '''
     
    def __init__(self, name, period, balancing_flag, x, y, imu, dbg=False):
        ''' @brief                  Constructs a user task.
            @details                Constructor block containing all of our input parameters for class Task_User
            @param name             The name of the task
            @param period           The period, in microseconds, between runs of 
                                    the task. 
            @param balancing_flag   Flag that indicates that the user has started the balancing program   
            @param x                Object for the x position of the ball 
            @param y                Object for the y position of the ball 
            @param imu              The imu object
        ''' 
        
        ## The name of the task
        self.name = name
        ## The period (in us) of the task
        self.period = period
        ## shares.Share object that indicates that the user has started the balancing program
        self.balancing_flag=balancing_flag
        ## shares.Share object for the x position of the ball
        self.x=x
        ## shares.Share object for the y position of the ball
        self.y=y
        ## The imu object
        self.imu = imu
        ## Debug flag
        self.dbg = dbg
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The state to run on the next iteration of the finite state machine
        self.state = 'S0_INIT'
        ## The number of runs of the state machine
        self.runs = 0
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    A finite state machine of 6 states. Allows for user input commands.
        '''
        ## The current time
        current_time = utime.ticks_us()
        #Runs only when self.next_time is met
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            
            # State 0 displays instruction message
            if self.state == 'S0_INIT':
                print('---------------------------------------------------\n'
                      'Use the following commands:\n'
                      '---------------------------------------------------\n'
                      'i        Calibrate IMU. \n'
                      'w        Write Calibration Coefficients for IMU. \n'
                      's        Start balancing. \n'
                      'c        Collect data for 30 seconds.\n'
                      'h or H   Print this help message. \n'
                      'Ctrl-c   Terminate Program. \n'
                      '---------------------------------------------------')
                self.transition_to('S1_IDLE')
                
            elif self.state == 'S1_IDLE':
                
                if (self.ser.any()):
                    char_in = self.ser.read(1).decode()
                    
                    if(char_in=='h' or char_in=='H'):
                        self.state='S0_INIT' 
                    
                    elif(char_in == 's'):
                        self.state = 'S2_BALANCING'
                
                    elif(char_in == 'c'):
                        print('Collecting data for 30 seconds...'
                              'Press x or X to end data collection early')
                        self.state = 'S3_COLLECT_DATA'
                        self.starttimeS3=current_time
                    
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_in))
            
            elif self.state == 'S2_BALANCING':
                self.balancing_flag.write(1)
                print('Balancing Initiating')
                self.state='S1_IDLE'
                
            elif self.state == 'S3_COLLECT_DATA':
                print("{},{},{}".format(utime.ticks_diff(current_time,self.starttimeS3)/1000000, self.x.read(), self.y.read())) 
                
                if (self.ser.any()):
                    char_s = self.ser.read(1).decode()
                    #ends data collection when user presses "x" or "X"
                    if (char_s == 'x' or char_s == 'X'):
                        self.state='S4_END_COLLECTION'
                    else:
                        print('Command \'{:}\' is invalid.'.format(char_s))
                #ends data collection when 10 seconds elapses
                elif utime.ticks_diff(current_time, self.starttimeS3) >= 10000000:
                        self.state='S4_END_COLLECTION'
            
            elif self.state == 'S4_END_COLLECTION':
                print ('Ending Data Collection...')
                self.state='S1_IDLE'

        
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state