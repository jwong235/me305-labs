''' @file               task_closedloop1.py
    @brief              A task operating a closed loop
    @details            Creates a class called Task_ClosedLoop that establishes
                        a finite state machine for a closed feedback loop
    @author             Jacob Wong
    @author             Kyle Hammer
    @date               December 9, 2021
'''

import utime

class Task_ClosedLoop():
    ''' @brief      Closed loop task
        @details    Implements a finite state machine for closed feedback loop.
    '''
    
    def __init__(self, name, period, CL_object,L_share, dbg=False):
        ''' @brief                  Constructs an closed loop task.
            @details                Constructor block containing all of our input parameters for class Task_ClosedLoop
            @param name             The name of the task
            @param period           The period, in microseconds, between runs of 
                                    the task.
            @param CL_object        A closed loop object
            @param L_share          A shares object holding the calculated actuation value
            @param dbg              A boolean flag used to enable or disable debug
                                    messages printed over the VCP
        '''
        ## The name of the task
        self.name=name
        ## The period, in microseconds, between runs of the task.
        self.period=period
        ## A closed loop object
        self.CL_object=CL_object
        ## A shares object holding the calculated actuation value
        self.L_share=L_share
        ## Debug flag
        self.dbg=dbg
        ## The state to run on the next iteration of the finite state machine
        self.state='S0_UPDATE'
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        ## The number of runs of the state machine
        self.runs = 0
        
    def run(self):
        ''' @brief      Runs one iteration of the FSM
            @details    Updates the CL_object and writes it to L_share
        '''  
        ## Current time 
        current_time = utime.ticks_us()
        if (utime.ticks_diff(current_time, self.next_time) >= 0):

            if self.state == 'S0_UPDATE':
                self.L = self.CL_object.update()
                self.L_share.write(self.L)
                
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            self.runs += 1
    
    def transition_to(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Optionally a debugging message can be printed
                        if the dbg flag is set when the task object is created.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state